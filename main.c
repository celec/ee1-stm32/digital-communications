#include <stdio.h>
#include "communications.h" // send_char(), send_short()
#include "uart.h" // contains uart_*.c prototypes
#include "spi_f1.h" // spi_txrx()
                    //
// XE1203 SPI trame
// 0 R/W A[4:0] D[7:0]
// if R: D = 1

int main(void) {
	int msk = (1 << 11) | (1 << 12) | (1 << 13);
    //char newline[3] = "\r\n\0";

	clock_setup();
	init_gpio();
	usart_setup();
    spi_clock_setup();
    spi_setup();

	while(1){
		led_set(msk);
		big_delay(100);

        // write to XE1203
        spi_cs_low();
        spi_txrx(0b00); // start sequence for write
        spi_txrx(0b00110); // SWParam
        // RX, 5dBm, linear, sigma-delta modulator 
        spi_txrx(0b10011001);
        spi_cs_hi();

		delay(100);
        //read from XE1203
        spi_cs_low();
        spi_txrx(0b01); // start sequence for read
        spi_txrx(0b00110); // SWParam
        spi_txrx(0xff); // dummies
        spi_cs_hi();

		led_clr(msk);
		big_delay(100);
    }
	
	return 0;
}
